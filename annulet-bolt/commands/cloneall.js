var program = require('commander'),
    _ = require('lodash'),
    async = require('async'),
    path = require('path'),
    util = require('util'),
    logger = require('winston'),
    git = require('nodegit');


module.exports = exports = function(repositories, destination, cb) {
    console.log('cloning all repositories to ' + destination);
    async.series(_.map(repositories, function(repository) {
        return function(cb) {

            //borrow options from nodegit test
            var opts = {
                ignoreCertErrors: 1,
                remoteCallbacks: {
                    credentials: function(url, userName) {
                        return git.Cred.sshKeyFromAgent(userName);
                    }
                }
            }
            var dirname = /\/(.+?)\.git/.exec(repository)[1];
            logger.info('cloning ' + repository + ' to ' + path.resolve(destination, dirname));
            git.Clone.clone(repository, path.resolve(destination, dirname), opts)
                .then(function(repo) {
                    logger.info(dirname + ' cloned.');
                    cb(null);
                })
                .catch(function(err) {
                    logger.error(err);
                    cb(null);
                });
        };
    }), function(err, r) {
        cb(err, r);
    });
};
