var program = require('commander'),
    _ = require('lodash'),
    async = require('async'),
    path = require('path'),
    util = require('util'),
    logger = require('winston'),
    git = require('nodegit');


module.exports = exports = function(repositories, destination, cb) {
    destination = destination || '.';
    console.log('pulling all repositories to ' + destination);
    async.series(_.map(repositories, function(repositoryName) {
        return function(cb) {
            var dirname = /\/(.+?)\.git/.exec(repositoryName)[1];
            logger.info('updating ' + dirname);

            var repository;
            git.Repository.open(path.resolve(destination, dirname))
                .then(function(repo) {
                    repository = repo;
                    return repo.fetch("origin", {
                        credentials: function(url, userName) {
                            return git.Cred.sshKeyFromAgent(userName);
                        }
                    });
                })
                .then(function() {
                    return repository.mergeBranches('master', 'origin/master');
                })
                .done(function() {
                    cb(null);
                });
        };
    }), function(err, r) {
        cb(err, r);
    });
};
