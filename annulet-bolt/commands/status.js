var program = require('commander'),
    _ = require('lodash'),
    async = require('async'),
    path = require('path'),
    util = require('util'),
    logger = require('winston'),
    fs = require('fs'),
    util = require('util'),
    git = require('nodegit');


module.exports = exports = function(repositories, destination, cb) {
    destination = destination || '.';
    console.log('statusing all repositories in ' + destination);
    async.series(_.map(repositories, function(repositoryName) {
        return function(cb) {
            var dirname = /\/(.+?)\.git/.exec(repositoryName)[1];

            try {
                var filestats = fs.lstatSync(path.resolve(destination, dirname));

                console.log('status for: ' + dirname);
                var repository;
                git.Repository.open(path.resolve(destination, dirname))
                    .then(function(repo) {
                        repo.getStatus()
                            .then(function(statuses) {
                                //pilfered from nodegit examples
                                function statusToText(status) {
                                    var words = [];
                                    if (status.isNew()) {
                                        words.push("NEW");
                                    }
                                    if (status.isModified()) {
                                        words.push("MODIFIED");
                                    }
                                    if (status.isTypechange()) {
                                        words.push("TYPECHANGE");
                                    }
                                    if (status.isRenamed()) {
                                        words.push("RENAMED");
                                    }
                                    if (status.isIgnored()) {
                                        words.push("IGNORED");
                                    }

                                    return words.join(" ");
                                }

                                statuses.forEach(function(file) {
                                    console.log("\t" + file.path() + " " + statusToText(file));
                                });
                            });
                    })
                    .done(function() {
                        cb(null);
                    });
            } catch (err) {
                //file does not exist, ignore.:
                cb(null);
            }
        };
    }), function(err, r) {
        cb(err, r);
    });
};
