var program = require('commander'),
    _ = require('lodash'),
    async = require('async'),
    path = require('path'),
    util = require('util'),
    logger = require('winston'),
    commands = require('./commands'),
    git = require('nodegit');

var list = function(v) {
    return v.split(',');
};

var repositories = [
    'git@bitbucket.org:rah-legacy-projects/annulet-marketing.git',
    'git@bitbucket.org:rah-legacy-projects/annulet-ui.git',
    'git@bitbucket.org:rah-legacy-projects/annulet-api.git',
    'git@bitbucket.org:rah-legacy-projects/annulet-models.git',
    'git@bitbucket.org:rah-legacy-projects/annulet-aux.git',
    'git@bitbucket.org:rah-legacy-projects/annulet-util.git',
    'git@bitbucket.org:rah-legacy-projects/annulet-id.git',
    'git@bitbucket.org:rah-legacy-projects/annulet-config.git',
    'git@bitbucket.org:rah-legacy-projects/annulet-auth-models.git',
    'git@bitbucket.org:rah-legacy-projects/annulet-seed.git',
    'git@bitbucket.org:rah-legacy-projects/annulet-content.git',
];

program
    .version('0.0.1');

program.command('cloneall [destination]')
    .description('clones all of the repositories at a given destination')
    .action(function(destination) {
       commands.cloneall(repositories, destination, function(err){
           console.log('clone complete.');
       });
    });

program.command('pullall [base directory]')
    .description('issues a pull for all of the repositories at a given destination')
    .action(function(destination){
        commands.pullall(repositories, destination, function(err){
            console.log('pull all complete.');
        });
    });

program.command('status [destination]')
.description('gets the status for a given directory')
.action(function(destination){
    destination = destination || '.';
    commands.status(repositories, destination, function(err){
        console.log('status all complete.');
    });
});

program.parse(process.argv);
