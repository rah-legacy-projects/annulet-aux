var path = require('path'),
    fs = require('fs'),
    async = require('async'),
    logger = require('winston'),
    jf = require('jsonfile'),
    child_process = require('child_process'),
    _ = require('lodash');

var options = {
    version: '0.11.0',
    path: '/home/ross/treasury_lane/annulet/forks'
};


async.waterfall([

        function(cb) {
            cb(null, {});
        },
        function(p, cb) {
            fs.readdir(options.path, function(err, files) {
                async.parallel(_.map(files, function(file) {
                    return function(cb) {
                        fs.stat(path.resolve(options.path, file), function(err, stat) {
                            if (stat.isDirectory()) {
                                cb(null, path.resolve(options.path, file));
                            }
                        });
                    };
                }), function(err, r) {
                    p.projectDirectories = r;
                    cb(err, p);
                });
            });
        },
        function(p, cb) {
            async.parallel(_.map(p.projectDirectories, function(directory) {
                return function(cb) {
                    fs.readdir(directory, function(err, files) {
                        var packagePath = _.find(files, function(file) {
                            return /package.json$/.test(file);
                        });
                        if (!!packagePath) {
                            logger.info('got package: ' + path.resolve(directory, packagePath));
                            return cb(null, path.resolve(directory, packagePath));
                        }
                        cb();
                    });
                };
            }), function(err, r) {
                p.packages = _.compact(r);
                cb(err, p);
            });

        },
        function(p, cb) {
            async.parallel(_.map(p.packages, function(packfile) {
                return function(cb) {
                    var pack = jf.readFileSync(packfile);
                    pack.version = options.version;
                    jf.writeFileSync(packfile, pack);
                    cb(null);
                };
            }), function(err, r) {
                cb(err, p);
            });
        },
        function(p, cb) {
            async.parallel(_.map(p.packages, function(packfile) {
                return function(cb) {
                    async.series([

                        function(cb) {
                            child_process.exec('git commit -m "' + options.version + '" package.json', {
                                cwd: path.dirname(packfile)
                            }, function(err, stdout, stderr) {
                                console.log(stdout);
                                console.log('!!!error: ' + stderr);
                                if (!!err) {
                                    logger.error(util.inspect(err));
                                }
                                cb(err);
                            });
                        },
                        function(cb) {
                            child_process.exec('git tag "' + options.version + '"', {
                                cwd: path.dirname(packfile)
                            }, function(err, stdout, stderr) {
                                console.log(stdout);
                                console.log('!!!error: ' + stderr);
                                if (!!err) {
                                    logger.error(util.inspect(err));
                                }
                                cb(err);
                            });
                        }
                    ], function(err, r) {
                        cb(err, r);
                    });
                }
            }), function(err, r) {
                logger.info('All projects commited pack and tagged.');
            });
        },
    ],
    function(err, p) {
        console.log('ta-daaa!');
    });
