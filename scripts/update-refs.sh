wmctrl -F -c 'Annulet UI'
wmctrl -F -c 'Annulet API'
wmctrl -F -c 'Annulet ID'
forever stopall

echo -e '\n stopped annulet.\n';

cd ../

echo -e '\n config\n';
cd ../annulet-config
#rm -rf node_modules/annulet-util
npm install ../annulet-util

echo -e '\n auth models\n';
cd ../annulet-auth-models
#rm -rf node_modules/annulet-config
#rm -rf node_modules/annulet-util
npm install ../annulet-config
npm install ../annulet-util

echo -e '\n id\n';
cd ../annulet-id
#rm -rf node_modules/annulet-config
#rm -rf node_modules/annulet-util
#rm -rf node_modules/gurarail-auth-models
npm install ../annulet-config
npm install ../annulet-util
npm install ../annulet-auth-models

echo -e '\n models\n';
cd ../annulet-models
#rm -rf node_modules/annulet-util
npm install ../annulet-util


echo -e '\n content\n';
cd ../annulet-content
#rm -rf node_modules/annulet-config
#rm -rf node_modules/annulet-util
#rm -rf node_modules/gurarail-models
npm install ../annulet-models
npm install ../annulet-config
npm install ../annulet-util

echo -e '\n api\n';
cd ../annulet-api
#rm -rf node_modules/annulet-config
#rm -rf node_modules/annulet-util
#rm -rf node_modules/annulet-models
#rm -rf node_modules/annulet-content
npm install ../annulet-config
npm install ../annulet-util
npm install ../annulet-models
npm install ../annulet-content

echo -e '\n ui\n';
cd ../annulet-ui
#rm -rf node_modules/annulet-config
#rm -rf node_modules/annulet-util
npm install ../annulet-config
npm install ../annulet-util

echo -e '\n seed\n';
cd ../annulet-seed
#rm -rf node_modules/annulet-config
#rm -rf node_modules/annulet-util
#rm -rf node_modules/annulet-models
#rm -rf node_modules/annulet-content
npm install ../annulet-config
npm install ../annulet-util
npm install ../annulet-models
npm install ../annulet-content

echo -e '\n test\n';
cd ../annulet-test
#rm -rf node_modules/annulet-config
#rm -rf node_modules/annulet-util
#rm -rf node_modules/annulet-models
#rm -rf node_modules/annulet-content
#rm -rf node_modules/annulet-api
#rm -rf node_modules/annulet-id
#rm -rf node_modules/annulet-ui
npm install ../annulet-config
npm install ../annulet-util
npm install ../annulet-models
npm install ../annulet-content
npm install ../annulet-id
npm install ../annulet-api
npm install ../annulet-ui

echo -e '\n cleaned and updated annulet. \n'

#come home
cd ../annulet-aux/scripts
./autostart.sh


echo -e '\n ===== internal references updated. ====== \n'
