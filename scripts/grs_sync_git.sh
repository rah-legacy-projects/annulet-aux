echo UPDATING THE MODELS...
cd annulet-models
git pull
cd ..
echo UPDATING THE API...
cd annulet-api
git pull
cd ..
echo UPDATING THE ID...
cd annulet-id 
git pull
cd ..
echo UPDATING THE UTIL...
cd annulet-util
git pull
cd ..
echo UPDATING THE UI...
cd annulet-ui
git pull
cd ..
echo UPDATING THE PRETTY UI...
cd annulet-ui-pretty
git fetch upstream
git checkout master
git merge upstream/master
cd ..
echo UPDATING THE AUX...
cd annulet-aux
git pull
echo GIT UPDATE COMPLETE.
echo UPDATING annulet-config...
cd ..
cd annulet-config
git pull

