cd ../../annulet-api
rm -rf node_modules
echo -e '\t api cleaned'
cd ../annulet-auth-models
rm -rf node_modules
echo -e '\t auth models cleaned'
cd ../annulet-config
rm -rf node_modules
echo -e '\t config cleaned'
cd ../annulet-id
rm -rf node_modules
echo -e '\t id cleaned'
cd ../annulet-models
rm -rf node_modules
echo -e '\t models cleaned'
cd ../annulet-ui
rm -rf node_modules
echo -e '\t ui cleaned'
cd ../annulet-util
rm -rf node_modules
echo -e '\t util cleaned'
cd ../annulet-seed
rm -rf node_modules
echo -e '\t seed cleaned'

echo 'node modules cleaned.'

cd ../annulet-util
npm install
git add .
git commit -am 'cleaning up modules'
git push
echo -e '\t util installed'

cd ../annulet-config
npm install
git add .
git commit -am 'cleaning up modules'
git push
echo -e '\t config installed'

cd ../annulet-auth-models
npm install
git add .
git commit -am 'cleaning up modules'
git push
echo -e '\t auth models installed'

cd ../annulet-id
npm install
git add .
git commit -am 'cleaning up modules'
git push
echo -e '\t id installed'

cd ../annulet-models
npm install
git add .
git commit -am 'cleaning up modules'
git push
echo -e '\t models installed'

cd ../annulet-api
npm install
git add .
git commit -am 'cleaning up modules'
git push
echo -e '\t api installed'

cd ../annulet-ui
npm install
git add .
git commit -am 'cleaning up modules'
git push
echo -e '\t ui installed'

cd ../annulet-seed
npm install
git add .
git commit -am 'cleaning up modules'
git push
echo -e '\t seed installed'


echo ' ====================== modules cleaned. =======================';
