pushd(){
  command pushd "$@" > /dev/null;
}
popd(){
  command popd "$@" > /dev/null;
}

npmln(){
  lib=$1;
  echo -e "\t\t adding link to $lib"
  npm ln $1 &> /dev/null
}

mkln(){
  lib=$1;
  declare -a deps=("${!2}");
  echo -e "\033[1;32m${lib}\033[0m";
  pushd $lib;
  if [ ! \( -e ~/.node_modules/lib/node_modules/${lib} \) ]
  then
    echo -e "\t\t linking $lib";
    npm ln &> /dev/null
  else
    echo -e "\t\t $lib already linked"
  fi
 
  if [ $#deps > 0 ]
  then
    for dep in "${deps[@]}"
    do
      npmln $dep;
    done
  fi

  popd;
}


pushd ..;
pushd ..;

declare -a args=();

mkln "annulet-multitenant" args[@]
mkln "annulet-util" args[@]

args=('annulet-util' 'annulet-multitenant');
mkln "annulet-multitenant-audit" args[@]

args=('annulet-util')
mkln "annulet-config" args[@]

args=('annulet-util' 'annulet-multitenant' 'annulet-multitenant-audit');
mkln "annulet-models" args[@]

args=('annulet-util' 'annulet-config')
mkln "annulet-auth-models" args[@]

args=('annulet-util' 'annulet-models' 'annulet-config')
mkln 'annulet-content' args[@]

args=('annulet-util' 'annulet-models' 'annulet-config' 'annulet-content')
mkln 'annulet-api' args[@]

args=('annulet-util' 'annulet-auth-models' 'annulet-config')
mkln 'annulet-id' args[@]

args=('annulet-util' 'annulet-config')
mkln 'annulet-ui' args[@]

args=('annulet-util' 'annulet-models' 'annulet-config' 'annulet-content' 'annulet-api' 'annulet-id' 'annulet-ui' 'annulet-auth-models' 'annulet-multitenant' 'annulet-multitenant-audit')
mkln 'annulet-test' args[@]

popd;
popd;
