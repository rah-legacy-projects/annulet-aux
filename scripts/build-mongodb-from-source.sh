#!/usr/bin/env bash
#
# Compile and install MongoDB with SSL support
# tested an works on Ubuntu 14.04 LTS x64
#


set -e
set -u
set -o pipefail
# set -x     #debugging


#
# CONFIGURATION SECTION
#
export MONGO_VERSION="r2.6.6"      # Which version of MongoDB, which corresponds to a git tag
                                    # You can list available versions  with `git tag | grep -v "rc"`
                                    # inside the mongo git respository

# Self-signed SSL Certificate settings
export SSL_HOSTNAME="${HOSTNAME}"   # Should be FQDN of your MongoDB server
export SSL_C="US"                   # Country
export SSL_ST="Tennessee"          # State
export SSL_O="Encode"              # Organization
export SSL_DAYS="3650"              # Valid for in days == 10 years


#
# PREREQUSITIES
#

if [ ! -d "/builds/${MONGO_VERSION}" ]; then
  # Control will enter here if directory doesn't exist.
  mkdir -p /builds/"${MONGO_VERSION}"/bin || exit 1
fi


# install all the prerequsite packages needed for compilation
export DEBIAN_FRONTEND=noninteractive
aptitude install build-essential scons git-core libssl-dev libboost-filesystem-dev \
                 libboost-program-options-dev libboost-system-dev libboost-thread-dev \
                 -q -y


# clone the source from github, and check out the proper release
git clone git://github.com/mongodb/mongo.git
cd /builds/mongo
git checkout "${MONGO_VERSION}"

#
# COMPILATION and INSTALLATION
#

scons all --64 --ssl -j4 --prefix=/builds/"${MONGO_VERSION}"/bin/
# move to /usr/bin after the build and verify

# run the post-install script which makes the mongo user, creates the data folder
# and sets various permissions
cd /builds/mongo/debian
chmod +x mongodb-org-server.postinst
./mongodb-org-server.postinst configure

# configure upstart and create base config file
cp mongodb.upstart /etc/init/mongodb.conf
cp mongodb.conf /etc

# create a logrotate script so our log files don't overflow
cat > /etc/logrotate.d/mongodb-server <<EOF
/var/log/mongodb/*.log {
  weekly
  rotate 10
  copytruncate
  delaycompress
  compress
  notifempty
  missingok
}
EOF


#
# CONFIGURE SSL
#

# generate a self-signed SSL key/cert
cd /etc/ssl

openssl req \
    -new \
    -newkey rsa:4096 \
    -x509 \
    -days ${SSL_DAYS} \
    -nodes \
    -subj "/C=${SSL_C}/ST=${SSL_ST}/O=${SSL_O}/CN=${SSL_HOSTNAME}" \
    -keyout mongodb.key \
    -out mongodb.crt

# configure mongo to use the key/cert
cat mongodb.key mongodb.crt > mongodb.pem

cat >> /etc/mongodb.conf <<EOF
sslOnNormalPorts = prefer
sslPEMKeyFile = /etc/ssl/mongodb.pem
EOF


#
# CLEANUP
#

cd ~
rm -rf /builds/mongo