cd ../../annulet-util
echo -e '---- util ----'
git status
cd ../annulet-config
echo -e '---- config ----'
git status
cd ../annulet-auth-models
echo -e '---- auth models ----'
git status
cd ../annulet-id
echo -e '---- id ----'
git status
cd ../annulet-models
echo -e '---- models ----'
git status
cd ../annulet-api
echo -e '---- api ----'
git status
cd ../annulet-ui
echo -e '---- ui ----'
git status
cd ../annulet-seed
echo -e '---- seed ----'
git status

echo ' ====================== status complete. =======================';
