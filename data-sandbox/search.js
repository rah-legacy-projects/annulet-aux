var models = require('annulet-models'),
    async = require('async'),
    config = require('annulet-config');

async.waterfall([

    function(cb) {
        models.modeler.setup(config.paths.dbListing(), cb);
    },
    function(cb) {
        models.modeler.db({
            collection: 'Customer',
            query: {
                _id: '54c1e92e3b1ba715333fe301'
            }
        }, cb);
    },
    function(models, cb) {
        var rex = /dd/i;
        models.complaints.Complaint.find({
            customer: '54c1e92e3b1ba715333fe301',
            $or: [{
                text: {$regex: rex}
            }, {
                complainerName: {$regex: rex}
            }, {
                status: {$regex: rex}
            }, {
                product: {$regex: rex}
            }, {
                subProduct: {$regex: rex}
            }, {
                issueType: {$regex: rex}
            }, {
                subIssueType: {$regex: rex}
            }, {
                state: {$regex: rex}
            }, {
                zip: {$regex: rex}
            }, {
                submittedVia: {$regex: rex}
            }, {
                companyResponseNote: {$regex: rex}
            }, {
                timelyResponse: {$regex: rex}
            }, {
                consumerDisputed: {$regex: rex}
            }, {
                active: {$regex: rex}
            }, {
                deleted: {$regex: rex}
            }]
        }).lean()
            .exec(function(err, complaints) {
                cb(err, models, {
                    complaints: complaints
                });
            });
    }

], function(err, models, p) {
    //console.log(JSON.stringify(p,null,4));
    console.log('complaints: ' + p.complaints.length);
});
